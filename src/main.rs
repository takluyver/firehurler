use dbus::blocking::Connection;
use std::convert::TryFrom;
use std::os::unix::ffi::OsStrExt;
use std::path::PathBuf;
use std::time::Duration;
use std::vec::Vec;

// https://crates.io/crates/dbus

// Comment from mozilla-central RemoteUtils.cpp:
// the commandline property is constructed as an array of int32_t
// followed by a series of null-terminated strings:
//
// [argc][offsetargv0][offsetargv1...]<workingdir>\0<argv[0]>\0argv[1]...\0

// (offset is from the beginning of the buffer)
fn make_cmdline(cwd: &PathBuf, args: Vec<String>) -> Vec<u8> {
    let mut buf = Vec::new();
    let mut nums: Vec<usize> = Vec::new();
    let start_offset: usize = 4 * (args.len() + 1);
    nums.push(args.len());
    buf.extend(cwd.as_os_str().as_bytes());
    buf.push(0);

    for arg in args.iter() {
        nums.push(start_offset + buf.len());
        buf.extend(arg.as_bytes());
        buf.push(0);
    }

    let mut res = Vec::with_capacity(start_offset);
    for num in nums {
        res.extend(&u32::try_from(num).unwrap().to_le_bytes())
    }
    res.extend(buf);
    res
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let conn = Connection::new_session()?;

    // Second, create a wrapper struct around the connection that makes it easy
    // to send method calls to a specific destination and path.
    let proxy = conn.with_proxy("org.mozilla.firefox.ZGVmYXVsdA__", "/org/mozilla/firefox/Remote", Duration::from_millis(5000));

    // Now make the method call. The ListNames method call takes zero input parameters and
    // one output parameter which is an array of strings.
    // Therefore the input is a zero tuple "()", and the output is a single tuple "(names,)".
    let cmdline = make_cmdline(&std::env::current_dir()?, std::env::args().collect());
    proxy.method_call("org.mozilla.firefox", "OpenURL", (cmdline,))?;

    Ok(())
}
